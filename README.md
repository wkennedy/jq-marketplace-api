# Atlassian Marketplace REST API and jq

Why struggle with difficult to decipher JSON when you can use a structured query to pyull data?

![](json.png)

In this post, we'll take a look at using the Marketplace API to find the top 20 Atlassian app add on by product; Jira, Confluence, Crowd, Bitbucket.

We'll focus on extracting the application key for using in another phase of our pipeline; specifying which apps to be loaded as system plugins.

# Top 20 Jira Addons
    curl https://marketplace.atlassian.com/rest/2/addons?application=jira&filter=highest-rated&limit=20
# Top 20 Confluence Addons
    curl https://marketplace.atlassian.com/rest/2/addons?application=confluence&filter=highest-rated&limit=20
# Top 20 Crowd Addons
    curl https://marketplace.atlassian.com/rest/2/addons?application=crowd&filter=highest-rated&limit=20
# Top 20 Bitbucket Addons
    curl https://marketplace.atlassian.com/rest/2/addons?application=bitbucket&filter=highest-rated&limit=20

## API Reference

Take a look at Atlassian's reference materials for the Market place API as your authoritative source. Using 'jq' on our data, we can get the following:

    jq 'keys' jira.20200224.json 

[
  "_embedded",
  "_links",
  "count"
]

Taking a more round about approach (jq . jira.20200224.json | cut -f1 -d: | sed 's/^[[:space:]]*//g' | sort -u) we can see the types of data available:

"addons"
"alternate"
"averageStars"
"banners"
"bundled"
"bundledCloud"
"byKey"
"categories"
"distribution"
"downloads"
"highRes"
"href"
"image"
"isAtlassian"
"key"
"logo"
"name"
"next"
"programs"
"query"
"reviews"
"self"
"smallHighResImage"
"smallImage"
"status"
"summary"
"tagLine"
"templated"
"topVendor"
"totalInstalls"
"totalUsers"
"type"
"unscaled"
"vendor"
"versions

Our focus is the application key.

## Example

This example will dump the data we need.

    jq -r ._embedded.addons[].key jira-top20.keys


com.mxgraph.jira.plugins.drawio
com.midori.jira.plugin.pdfview
com.fca.jira.plugins.workflowToolbox.workflow-toolbox
com.midori.jira.plugin.betterexcel
com.xpandit.plugins.jiraxporter
com.codebarrel.addons.automation
com.herocoders.plugins.jira.issuechecklist-free
ru.mail.jira.plugins.mailrucal
io.aha.connect
com.activitytimeline.plugin
com.onresolve.jira.groovy.groovyrunner
agile.estimation.3.0_private
com.qotilabs.jira.rich-filters-plugin
com.resolution.atlasplugins.samlsso.Jira
com.xpandit.plugins.xray
com.kanoah.test-manager
com.eazybi.jira.plugins.eazybi-jira
com.keplerrominfo.jira.plugins.jjupin
com.xiplink.jira.git.jira_git_plugin
com.spartez.jira.plugins.bork.tfs4jira

Similarly, we can get the corresponding names:

    jq -r ._embedded.addons[].name jira.20200224.json

draw.io Diagrams for Jira
Better PDF Exporter for Jira
Jira Workflow Toolbox
Better Excel Exporter for Jira (XLSX)
Xporter - Export issues from Jira
Automation for Jira
Issue Checklist Free
My.com Calendar
Aha! roadmaps for Jira
Activity Timeline for Jira Server
ScriptRunner for Jira
Planning Poker ®
Rich Filters for Jira Dashboards
SAML Single Sign On (SSO) Jira, SAML/SSO
Xray Test Management for Jira
TM4J - Test Management for Jira
eazyBI Reports and Charts for Jira
Power Scripts™ - Jira script automation
Git Integration for Jira
TFS4JIRA Azure DevOps integration

### More

Run the addons.sh script to get the top 20 addons by key. Modify to suit your need - filter for Free addons, by Category and more!


