# https://confluence.atlassian.com/confkb/how-to-get-a-list-of-confluence-apps-and-their-licensing-information-959301808.html
## Final report is saved on file /tmp/PluginReport.csv
### Change these arguments according to your instance
CONFLUENCE_SERVER_URL="http://localhost:6663"
CONFLUENCE_SERVER_CONTEXT_PATH="/c663"
ADMIN_USRNAME=admin
ADMIN_PWD=admin


CONFLUENCE_BASE_URL=$CONFLUENCE_SERVER_URL$CONFLUENCE_SERVER_CONTEXT_PATH
curl --user $ADMIN_USRNAME:$ADMIN_PWD $CONFLUENCE_BASE_URL/rest/plugins/latest/ 2>/dev/null | jq -r '.plugins[] | select(.userInstalled) | "\(.name):\(.version):\(.vendor.name):\(.usesLicensing):\(.links.self)"' > /tmp/restapi.out
echo "Plugin Name:Plugin Version:Vendor Name:Use License:License Type:Expiry Date:License Key:SEN" > /tmp/PluginReport.csv
while read PLUGINLINE; do
PLUGINKEY=$(awk 'BEGIN { FS=":" } { print $5 }' <<< "$PLUGINLINE")
LICINFO=$(curl --user $ADMIN_USRNAME:$ADMIN_PWD $CONFLUENCE_SERVER_URL$PLUGINKEY/license/ 2>/dev/null | jq '"\(.licenseType):\(.maintenanceExpiryDateString):\(.rawLicense):\(.supportEntitlementNumber)"' | sed -e 's/^"//' -e 's/"$//')
PLGINFO=$(awk 'BEGIN { FS=":"; OFS=":"; } { print $1,$2,$3,$4 }' <<< "$PLUGINLINE")
echo "$PLGINFO:$LICINFO"
done < /tmp/restapi.out >> /tmp/PluginReport.csv
