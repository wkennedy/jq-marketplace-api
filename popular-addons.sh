#!/bin/bash
# Find and export the top addons from Atlassian Marketplace
# William Kennedy <william@a9group.net>
# Wed 19 Aug 2020 09:25:37 AM PDT

# Top 20 Jira Addons
    curl  -o jira.$(date +%Y%m%d).json "https://marketplace.atlassian.com/rest/2/addons?application=jira&filter=popular&limit=20"
# Top 20 Confluence Addons
    curl -o confluence.$(date +%Y%m%d).json "https://marketplace.atlassian.com/rest/2/addons?application=confluence&filter=popular&limit=20"
# Top 20 Crowd Addons
    curl -o crowd.$(date +%Y%m%d).json "https://marketplace.atlassian.com/rest/2/addons?application=crowd&filter=popular&limit=20"
# Top 20 Bitbucket Addons
    curl -o bitbucket.$(date +%Y%m%d).json "https://marketplace.atlassian.com/rest/2/addons?application=bitbucket&filter=popular&limit=20" 

#jq -r ._embedded.addons[].key 

echo "Jira"
jq -r ._embedded.addons[].name jira.$(date +%Y%m%d).json | tee -a jira-most-popular-20.$(date +%Y%m%d).name
echo "------------------------"

echo
echo "Confluence"
jq -r ._embedded.addons[].name confluence.$(date +%Y%m%d).json | tee -a confluence-top-20.$(date +%Y%m%d).name
echo "------------------------"

echo
echo "Crowd"
jq -r ._embedded.addons[].name crowd.$(date +%Y%m%d).json | tee -a crowd-top-20.$(date +%Y%m%d).name
echo "------------------------"

echo
echo "Bitbucket"
jq -r ._embedded.addons[].name bitbucket.$(date +%Y%m%d).json | tee -a bitbucket-top-20.$(date +%Y%m%d).name
echo "------------------------"
echo
